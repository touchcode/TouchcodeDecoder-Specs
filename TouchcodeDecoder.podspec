#
# Be sure to run `pod lib lint TouchcodeDecoder.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TouchcodeDecoder'
  s.version          = '1.0.7'
  s.summary          = 'Enable decoding of touchcodes within iOS applications.'
  s.description      = 'The TouchcodeDecoder pod easily enables the ability to decode touchcodes directly within an iOS application. Simply set the view to collect touches and the SDK handles the rest.'

  s.homepage         = 'https://gitlab.com/touchcode/TouchcodeSDK'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Touchcode Inc.' => 'support@touchcode.com' }
  s.source           = { :git => 'https://gitlab.com/touchcode/TouchcodeDecoder.git', :tag => '1.0.8' }
  s.swift_versions   = '5.0'

  s.ios. vendored_frameworks = ['TouchcodeDecoder.xcframework']

  s.dependency 'Alamofire', '4.9.1'
  s.dependency 'NetUtils', '4.1.0'
  s.dependency 'CryptoSwift', '1.3.3'
  
  s.ios.deployment_target = '10.0'
  
end